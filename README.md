# Backstage Repository

This repository houses the following -
- Backstgae backend code
- Backstage frontend code
- Terraform code to provision:
  - Base Layer:
    
    Comprises of creation of Azure Container Registry and Service Account for Gitlab, AKS and Data Reg
  - Main Layer:
    
    Comprises of creation of AKS cluster and ancilary components
- Kubernetes namespace, deployment, secret and service yaml files
- Templates for gitlab-ci pipeline
- Dockerfiles for backstage frontend and backend
- Nginx config which is used to host backstage frontend