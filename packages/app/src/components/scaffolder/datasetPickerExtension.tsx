/*
 * Copyright 2021 The Backstage Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React from 'react';
import type { FieldValidation } from '@rjsf/core';
import {
  createScaffolderFieldExtension,
  FieldExtensionComponentProps,
  scaffolderPlugin,
} from '@backstage/plugin-scaffolder';
import { useApi } from '@backstage/core-plugin-api';
import {
  catalogApiRef,
  humanizeEntityRef,
} from '@backstage/plugin-catalog-react';
import { Chip, TextField } from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { useEffect } from 'react';
import useAsync from 'react-use/lib/useAsync';

/**
 * The input props that can be specified under `ui:options` for the
 * `EntityPicker` field extension.
 *
 * @public
 */
export interface EntityPickerUiOptions {
  allowedKinds?: string[];
  defaultKind?: string;
  allowArbitraryValues?: boolean;
}

export const DatasetEntityPicker = (
  props: FieldExtensionComponentProps<string, EntityPickerUiOptions>,
) => {
  const {
    onChange,
    schema: { title = 'Dataset Entity', description = 'An dataset entity from the catalog' },
    required,
    uiSchema,
    rawErrors,
    formData,
    idSchema,
  } = props;
  const defaultKind = uiSchema['ui:options']?.defaultKind;

  const catalogApi = useApi(catalogApiRef);

  const { value: entities, loading } = useAsync(() =>
    catalogApi.getEntities(
      { filter: { kind: 'Component', 'spec.type': 'ada.table' } }
    ),
  );

  const entityRefs = entities?.items.map(e =>
    humanizeEntityRef(e),
  );

  useEffect(() => {
    if (entityRefs?.length === 1) {
      onChange(entityRefs[0]);
    }
  }, [entityRefs, onChange]);

  return (
    <FormControl
      margin="normal"
      required={required}
      error={rawErrors?.length > 0 && !formData}
    >
      <Autocomplete
        multiple
        id={idSchema?.$id}
        value={formData ? formData.split(',') : []}
        options={entityRefs?.map((ref) => ref) || []}
        disabled
        renderTags={(value, getTagProps) =>
          value.map((option, index) => (
            <Chip variant="default"
            label={option} 
            deleteIcon={<></>}
            disabled {...getTagProps({ index })} />
          ))}
        renderInput={(params) => (
          <TextField {...params} label="Datasets" placeholder="Datasets from basket"  />
        )}
      />
    </FormControl>
  );
};
export const DatasetEntityPickerFieldExtension = scaffolderPlugin.provide(
  createScaffolderFieldExtension({
    name: 'DatasetEntityPicker',
    component: DatasetEntityPicker,
    validation: () => {
      return true
    },
  }),
);