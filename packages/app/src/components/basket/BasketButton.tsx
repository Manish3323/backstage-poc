
/*
 * Copyright 2022 The Backstage Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { useEntity } from "@backstage/plugin-catalog-react";
import { Grid, Button } from "@material-ui/core";
import React from "react";
import { useBasketMapForEntity } from "./useBasketMap";

export const BasketButton = () => {
  const { entity } = useEntity();
  const { addedInBasketAlready, save, remove } = useBasketMapForEntity(entity);
  return (
    <Grid
      item
      md={12}
      xs={12}
      style={{ justifyContent: 'flex-end', display: 'grid' }}
    >
      {addedInBasketAlready ? (
        <Button variant="contained" color="primary" onClick={remove}>
          Remove from basket
        </Button>
      ) : (
        <Button variant="contained" color="primary" onClick={save}>
          Add to basket
        </Button>
      )}
    </Grid>
  );
};
