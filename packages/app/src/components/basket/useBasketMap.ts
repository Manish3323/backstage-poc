/*
 * Copyright 2022 The Backstage Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Entity } from '@backstage/catalog-model';
import { WebStorage } from '@backstage/core-app-api';
import { errorApiRef, useApi } from '@backstage/core-plugin-api';
import { useState } from 'react';

export type BasketItem = {
  system: string;
  format: string;
  subComponentOf: string;
  name: string;
};
export type BasketMap = Map<string, BasketItem>;

export const useBasketMap = () => {
  const errorRef = useApi(errorApiRef);
  const storage = WebStorage.create({
    errorApi: errorRef,
    namespace: 'ada-backstage',
  });
  const maybeBasket = storage.get('basket') as BasketMap;
  const [basketMap, setBasketMap] = useState<BasketMap>(
    maybeBasket
      ? new Map(Object.entries(storage.get('basket') as BasketMap))
      : new Map(),
  );
  const reset = () => {
    setBasketMap(new Map(Object.entries(storage.get('basket') as BasketMap)));
  };

  const removeFromBasket = async (entity: Entity) => {
    basketMap.delete(entity.metadata.name);
    await storage.set('basket', Object.fromEntries(basketMap));
    reset();
  };
  const saveToBasket = async (entity: Entity) => {
    const map: BasketMap | undefined = storage.get('basket');
    const objectToBeSaved = {
      ...map,
      [entity.metadata.name]: {
        system: entity.spec?.system,
        format: entity.spec?.format,
        subComponentOf: entity.spec?.subcomponentOf,
        name: entity.metadata.name,
      },
    };
    await storage.set('basket', objectToBeSaved);
  };
  return { removeFromBasket, saveToBasket, basketMap };
};

export const useBasketMapForEntity = (entity: Entity) => {
  const { basketMap, saveToBasket, removeFromBasket } = useBasketMap();
  const [addedInBasketAlready, setAddedInBasket] = useState(
    !!basketMap?.get(entity.metadata.name),
  );
  const save = async () => {
    saveToBasket(entity);
    setAddedInBasket(true);
  };
  const remove = async () => {
    removeFromBasket(entity);
    setAddedInBasket(false);
  };

  return { basketMap, remove, save, addedInBasketAlready };
};
