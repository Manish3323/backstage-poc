/*
 * Copyright 2021 The Backstage Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Entity } from '@backstage/catalog-model';
import { WebStorage } from '@backstage/core-app-api';
import {
  Content,
  Header,
  Link,
  Page,
  Table,
  TableProps,
} from '@backstage/core-components';
import { configApiRef, errorApiRef, useApi } from '@backstage/core-plugin-api';
import {
  catalogApiRef,
  columnFactories,
} from '@backstage/plugin-catalog-react';
import { Button, Grid, makeStyles, Paper, Theme } from '@material-ui/core';
import RemoveShoppingCartIcon from '@material-ui/icons/RemoveShoppingCart';
import React, { useEffect, useState } from 'react';
import { useBasketMap, useBasketMapForEntity } from './useBasketMap';

export type BasketItem = {
  system: string;
  format: string;
  subComponentOf: string;
  name: string;
};
export type BasketMap = Map<string, BasketItem>;

export const BasketPage = () => {
  const [list, setList] = useState<Entity[]>([]);
  const catalogApi = useApi(catalogApiRef);
  const configApi = useApi(configApiRef);
  const { basketMap, removeFromBasket } = useBasketMap();
  const baseUrl = configApi.getString('app.baseUrl');
  const validBaseUrl = new URL(baseUrl);
  const publicPath = validBaseUrl.pathname.replace(/\/$/, '');

  useEffect(() => {
    const names = [...basketMap.keys()];
    if (names.length > 0) {
      catalogApi
        .getEntities({
          filter: [
            {
              kind: ['Component'],
              'spec.type': 'ada.table',
              'metadata.name': names,
            },
          ],
        })
        .then(r => setList(r.items));
    }
  }, [catalogApi, basketMap]);

  const columns = [
    columnFactories.createEntityRefColumn({
      defaultKind: 'Component',
    }),
    columnFactories.createOwnerColumn(),
    columnFactories.createSystemColumn(),
    columnFactories.createSpecTypeColumn(),
  ];

  const defaultActions: TableProps<Entity>['actions'] = [
    entity => {
      return {
        icon: () => <RemoveShoppingCartIcon />,
        tooltip: 'Remove from basket',
        onClick: () => removeFromBasket(entity),
      };
    },
  ];

  const templatePath = `${`/${publicPath}`}/create/templates/default/create-data-product-template?formData=${JSON.stringify(
    {
      entities: [
        ...list.map(
          x => `${x.spec?.system}/${x.spec?.subcomponentOf}/${x.metadata.name}`,
        ),
      ].join(','),
    },
  )}`;

  return (
    <Page themeId="home">
      <Header
        title="Datasets Basket"
        subtitle="Datasets shown below can be used to create data projects"
      />
      <Content>
        <Grid container direction="row">
          <Grid
            item
            xs={12}
            style={{ display: 'flex', justifyContent: 'flex-end' }}
          >
            <Link to={templatePath}>
              <Button variant="contained" color="primary">
                Checkout
              </Button>
            </Link>
          </Grid>
          <Grid item xs>
            <Table<Entity>
              columns={columns}
              title={`Basket (${list.length})`}
              data={list}
              actions={defaultActions}
            />
          </Grid>
        </Grid>
      </Content>
    </Page>
  );
};
