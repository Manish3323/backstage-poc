/*
 * Copyright 2020 The Backstage Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { createTemplateAction } from '@backstage/plugin-scaffolder-backend';
import { Config } from '@backstage/config';

import {
  AdaIngestionServiceClient,
  SourceConfig,
} from '../../../grpcClient/ada_ingestion_service';
import { ChannelCredentials } from '@grpc/grpc-js';
import { CatalogApi } from '@backstage/catalog-client';

const readAdaServerConfig = (config: Config) => {
  const adaServer = config.getConfig('ada-server');
  const host = adaServer.getString('host');
  const port = adaServer.getString('port');
  return { host, port };
};

const insureContext = ChannelCredentials.createInsecure();

export const datasetRegistrationAction = (
  catalogClient: CatalogApi,
  config: Config,
) => {
  const conf = readAdaServerConfig(config);
  const client = new AdaIngestionServiceClient(
    `${conf.host}:${conf.port}`,
    insureContext,
  );

  return createTemplateAction<{
    description: string;
    owner: string;
    lobContainerName: string;
    dataset: string;
    format: string;
  }>({
    id: 'ubs:register-dataset',
    schema: {
      input: {
        required: [
          'dataset',
          'description',
          'owner',
          'lobContainerName',
          'format',
        ],
        type: 'object',
        properties: {
          dataset: {
            type: 'string',
            title: 'Name',
            description: 'The name of the dataset',
          },
          description: {
            type: 'string',
            title: 'description',
            description:
              'The description of the dataset that will be registered in ADA.',
          },
          owner: {
            type: 'string',
            title: 'owner',
            description:
              'The owner of the dataset that will be registered in ADA.',
          },
          lobContainerName: {
            type: 'string',
            title: 'LOB',
            description: 'The LOB/ Azure Container to which dataset belong to.',
          },
          format: {
            type: 'string',
            title: 'format',
            description: 'Format of the datasets to be registered.',
          },
        },
      },
    },
    async handler(ctx) {
      await new Promise((resolve, reject) => {
        const sourceConfig = new SourceConfig({
          lob: ctx.input.lobContainerName,
          dataset: ctx.input.dataset,
          format: ctx.input.format,
          description: ctx.input.description,
          owner: ctx.input.owner
        });
        ctx.logger.info(`Loading dataset using ada registration service with message : ${sourceConfig.toString()}`)
        client.LoadDataset(
          sourceConfig,
          async (err, response) => {
            if (err) reject(err);
            else if (response) {
              try {
                ctx.logger.info(`Finished Loading dataset using ada registration service.`)
                ctx.logger.info(`Adding dataset to backstage catalog.`)
                await catalogClient.addLocation(
                  {
                    type: 'url',
                    target: response.backstage_entity_url,
                  },
                  ctx.secrets?.backstageToken
                    ? { token: ctx.secrets.backstageToken }
                    : {},
                );
              ctx.logger.info(`Finished adding dataset to backstage catalog.`)
              }catch(e) {
                reject(`error ${e.message}`)
              }
              ctx.output('entityRef', `component:default/${ctx.input.dataset}`);
              resolve(response)
            }
          },
        );
      });
    },
  });
};
