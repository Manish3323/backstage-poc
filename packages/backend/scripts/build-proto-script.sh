#!/bin/bash

BASEDIR=$(dirname "$0")
cd ${BASEDIR}/..

PROTOBUF_PATH=src/protobuf

PROTO_DEST=src/grpcClient
mkdir -p ${PROTO_DEST}


echo $PROTO_DEST
# TypeScript code generation
yarn run grpc_tools_node_protoc \
    --plugin=protoc-gen-ts=./node_modules/.bin/protoc-gen-ts \
    --ts_out=${PROTO_DEST} \
    -I ${PROTOBUF_PATH} \
    ada_ingestion_service.proto


# # JavaScript code generation
# yarn run grpc_tools_node_protoc \
#     --js_out=import_style=commonjs,binary:${PROTO_DEST} \
#     --grpc_out=${PROTO_DEST} \
#     --plugin=protoc-gen-grpc=./node_modules/.bin/grpc_tools_node_protoc_plugin \
#     -I ./proto \
#     proto/*.proto