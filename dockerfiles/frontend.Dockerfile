#Stage 1
FROM node:14-buster AS build


ARG ARG_APIM_ENDPOINT
ARG ARG_FRONTEND_APIM_ENDPOINT
#ARG ARG_ENV

ENV APIM_ENDPOINT=$ARG_APIM_ENDPOINT
ENV FRONTEND_APIM_ENDPOINT=$ARG_FRONTEND_APIM_ENDPOINT
#ENV ENV=$ARG_ENV

#RUN echo $ENV



RUN mkdir -p /packages/app && mkdir /plugins

COPY packages/app /packages/app
COPY plugins /plugins/.
COPY tsconfig.json package.json lerna.json app-config.yaml /

#RUN sed -i "s|ENV_PLACEHOLDER|$ARG_ENV|g" /app-config.yaml

RUN cat app-config.yaml

RUN yarn workspace backstage-frontend install
RUN yarn workspace backstage-frontend build 

#Stage 2
FROM nginx:mainline

RUN apt-get update && apt-get -y install procps jq curl && rm -rf /var/lib/apt/lists/*

COPY --from=build /packages/app/dist /usr/share/nginx/html
COPY nginx-conf/config.template /etc/nginx/templates/default.conf.template

ENV PORT 80
