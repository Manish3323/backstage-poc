# Resource Group Creation
resource "azurerm_resource_group" "common-resource-group" {
  name     = "common-${var.prefix}-rg"
  location = var.region
}

#ACR creation
resource "azurerm_container_registry" "common-acr" {
  name                = "common${var.prefix}ACR"
  resource_group_name = azurerm_resource_group.common-resource-group.name
  location            = azurerm_resource_group.common-resource-group.location
  sku                 = "Basic"
}

# Create variable file for base project
resource "local_file" "base-variable-file" {
    content = yamlencode({"variables": {
      ACR_URL: azurerm_container_registry.common-acr.login_server,
      TENANT_ID: data.azuread_client_config.current.tenant_id,
      APIM_ENDPOINT: data.azurerm_api_management.apim.gateway_url
    }})
    filename = "${path.module}/../../templates/base-variables.yaml"
}