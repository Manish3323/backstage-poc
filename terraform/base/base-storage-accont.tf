# #Storage account creation
# resource "azurerm_storage_account" "adls-storage-account" {
#   count = 2
#   depends_on = [
#     random_string.storage-account-name #[count.index]
#   ]
#   name                     = random_string.storage-account-name[count.index].id
#   resource_group_name      = azurerm_resource_group.common-resource-group.name
#   location                 = azurerm_resource_group.common-resource-group.location
#   account_tier             = "Standard"
#   account_replication_type = "LRS"
#   account_kind             = "StorageV2"
#   is_hns_enabled           = "true"

#   tags = {
#     ENV = "${terraform.workspace}"
#   }
# }

# ADLS filesystem creation
# TODO -
# move to modules so that it becomes resuable
# Discuss wih Murali about ACLs whether to be given at container level
# resource "azurerm_storage_data_lake_gen2_filesystem" "adls-filesystem" {
#   for_each           = toset(var.domains)
#   name               = each.value
#   storage_account_id = azurerm_storage_account.adls-storage-account.id
#   # owner = data.azuread_client_config.current.object_id

#   properties = {
#     hello = "aGVsbG8="
#   }
# }

# # #ADLS filesystem path management
# # # resource "azurerm_storage_data_lake_gen2_path" "adls-filesystem" {
# # #   for_each           = toset(var.domains)
# # #   path               = each.value
# # #   filesystem_name    = azurerm_storage_data_lake_gen2_filesystem.adls-filesystem.name
# # #   storage_account_id = azurerm_storage_account.adls-storage-account.id
# # #   resource           = "directory"
# # # owner = data.azuread_client_config.current.object_id

# # # ace {
# # #   permissions = "rwx"
# # #   scope = "access"
# # #   type ="user"
# # #   id = azuread_service_principal.application-sp[1].id
# # # }
# # #  ace {
# # #   permissions = "r--"
# # #   scope = "access"
# # #   type ="user"
# # #   id = azuread_service_principal.application-sp[0].id
# # # }
# # #}
