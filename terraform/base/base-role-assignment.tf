# Grant Gitlab usr ACR push on ACR
resource "azurerm_role_assignment" "role-assignment-gitlab-user-acr" {
  principal_id                     = azuread_service_principal.application-sp[1].id #objectId of Gitlab Service Principal
  role_definition_name             = "AcrPush"
  scope                            = azurerm_container_registry.common-acr.id
  skip_service_principal_aad_check = true
}

#Grant AKS user AcrPull acess on ACR
resource "azurerm_role_assignment" "role-assignment-aks-user-acr" {
  principal_id                     = azuread_service_principal.application-sp[0].id #objectId of AKS Service Principal
  role_definition_name             = "AcrPull"
  scope                            = azurerm_container_registry.common-acr.id
  skip_service_principal_aad_check = true
}

# #Grant Data reg user Blob storage acess on storage containers
# resource "azurerm_role_assignment" "role-assignment-data-reg-adls1" {
#   principal_id                     = azuread_service_principal.application-sp[2].id #objectId of Data Reg Service Principal
#   role_definition_name             = "Storage Blob Data Contributor"
#   scope                            = azurerm_storage_account.adls-storage-account[0].id
#   skip_service_principal_aad_check = true
# }

# #Grant Data reg user Blob storage acess on storage containers
# resource "azurerm_role_assignment" "role-assignment-data-reg-adls2" {
#   principal_id                     = azuread_service_principal.application-sp[2].id #objectId of Data Reg Service Principal
#   role_definition_name             = "Storage Blob Data Contributor"
#   scope                            = azurerm_storage_account.adls-storage-account[1].id
#   skip_service_principal_aad_check = true
# }