data "azuread_client_config" "current" {}

data "azurerm_api_management" "apim" {
  name                = "backstagepocapim"
  resource_group_name = "poc_rg"
}