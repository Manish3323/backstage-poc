# Service principals creation
#data "azuread_client_config" "current" {}

resource "azuread_application" "application" {
  count        = length(var.service_principal_types)
  display_name = "common-backstage-${lookup(element(var.service_principal_types, count.index), "name")}"
  owners       = [data.azuread_client_config.current.object_id]
}

resource "azuread_service_principal" "application-sp" {
  count                        = length(var.service_principal_types)
  application_id               = azuread_application.application[count.index].application_id
  app_role_assignment_required = false
  owners                       = [data.azuread_client_config.current.object_id]
}

