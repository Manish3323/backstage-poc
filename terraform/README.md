Queries 

# APIM 
# 1. Deploy service with internal load balancer in AKS
# 2. Peer aks vnet with apim vnet
#     a. Ping the url from a machine in the same VNET -> Aks MS why it takes so much of time for the endpoint to be accessible intra/inter vnet
# 3. Create routing rules in apim
# 4. In app-config.yaml for baseurl configuyre apim endpoint
# 5. Create get and post method
# 6. Create NSG with 
#    - inboud only from the peered network and only on the ports where the service is runnig


# #TODO
# # ssl disable (NOT FOR PROD)
# # firewall rules allow access from certain IPS
# # DDL to create a table .sql file
# # Add contributor permission of the aks service principal on resource group so that it can read the public ip
# # explore APIM for ssl termination
# # Move service principal to standard non de3stroyed code




# Manual Steps -

1. Create AKS cluster
2. Create a Vnet and Subnet for AKS cluster
3. Create APIM
4. Create a Vnet for APIM
5. Create NSG for both the subnets
6. Allow all from anywhere in Inboud rule
7. Allow all to anywhere in Outbound rule
8. Peer the AKS vnet and APIM vnet
9. Create backend service with internal load balancer service in AKS
10. Deploy front end in staticWebapp
11. Configure the app-config.yaml to point to APIM https endpoint as backend
12. Configure the backend port and url in app-config.yaml
13. Create a VM in AKS Vnet to test the availability of backend service endpoint
14. Create a VM in peered network ie APIM Vnet to test the availability of the backend service endpoint
15. Configure the routing rules in APIM for GET and POST for /api/*
16. Add CORS policy in APIM allowing * as origin