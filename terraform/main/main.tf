# Resource Group Creation
resource "azurerm_resource_group" "resource-group" {
  name     = "${terraform.workspace}-${var.prefix}-rg"
  location = var.region

  tags = {
    ENV = "${terraform.workspace}"
    Created_by = "Terraform"
  }
}

# #Static WebApp creation
# resource "azurerm_static_site" "staticwebapp" {
#   name                = "${terraform.workspace}-${var.prefix}-staticwebapp"
#   resource_group_name = azurerm_resource_group.resource-group.name
#   location            = "eastus2"
#   sku_tier = "Free"
#   sku_size = "Free"

#   tags = {
#     ENV = "${terraform.workspace}"
#     Created_by = "Terraform"
#   }
# }

# resource "azurerm_resource_group_template_deployment" "frontend_appsettings" {
#   deployment_mode     = "Incremental"
#   name                = "frontend-appsettings"
#   resource_group_name = azurerm_resource_group.resource-group.name

#   template_content = file("templates/staticwebapp-arm-appsettings.json")
#   parameters_content = jsonencode({
#     staticSiteName = {
#       value = azurerm_static_site.staticwebapp.name
#     },
#     appSetting1 = {
#       value = azurerm_public_ip.public-ip[0].ip_address
#     }
#   })
# }

# # Public IP creation
# resource "azurerm_public_ip" "public-ip" {
#   count = 3
#   name                = "${terraform.workspace}-${var.prefix}-pub-ip-${count.index}"
#   resource_group_name = azurerm_resource_group.resource-group.name
#   location            = azurerm_resource_group.resource-group.location
#   allocation_method   = "Static"
#   sku                 = "Standard"

#   tags = {
#     ENV = "${terraform.workspace}"
#   }
# }

# Azure Kunernetes Servie cluster creation
resource "azurerm_kubernetes_cluster" "aks-cluster" {

  name                = "${terraform.workspace}-${var.prefix}-aks"
  location            = azurerm_resource_group.resource-group.location
  resource_group_name = azurerm_resource_group.resource-group.name
  dns_prefix          = var.prefix

  default_node_pool {
    name           = "${var.prefix}np"
    node_count     = "1"
    vm_size        = "standard_d2_v2"
    vnet_subnet_id = azurerm_subnet.subnet.id
  }

  service_principal {
    client_id     = data.azuread_service_principal.aks-user.application_id
    client_secret = azuread_service_principal_password.aks-sp-password.value
  }

  network_profile {
    network_plugin    = "kubenet"
    load_balancer_sku = "standard"
    # load_balancer_profile {
    #   outbound_ip_address_ids = [for ip in azurerm_public_ip.public-ip[*]: ip.id]
    # }
  }

  tags = {
    ENV = "${terraform.workspace}"
    Created_by = "Terraform"
  }
}




# # # PSQL server creation
# # resource "azurerm_postgresql_server" "psql-server" {

# # depends_on = [
# #   random_password.psql-password
# # ]
# #   name                = "${terraform.workspace}-${var.prefix}-psql-server"
# #   location            = "eastus"
# #   resource_group_name = azurerm_resource_group.resource-group.name

# #   administrator_login          = var.psql_user
# #   administrator_login_password = random_password.psql-password.result

# #   sku_name   = "GP_Gen5_4"
# #   version    = "11"
# #   storage_mb = 5120

# #   backup_retention_days        = 7
# #   geo_redundant_backup_enabled = false #Set to true in PROD workloads
# #   auto_grow_enabled            = false #Set to True in PROD workloads

# #   public_network_access_enabled    = false
# #   ssl_enforcement_enabled          = false
# #   #ssl_minimal_tls_version_enforced = "TLS1_2"

# #   tags = {
# #     ENV = "${terraform.workspace}"
# #   }
# # }

# # # PSQL database creation
# # resource "azurerm_postgresql_database" "psql-db" {
# #   name                = "${terraform.workspace}-${var.prefix}-psql-db"
# #   resource_group_name = azurerm_resource_group.resource-group.name
# #   server_name         = azurerm_postgresql_server.psql-server.name
# #   charset             = "UTF8"
# #   collation           = "English_United States.1252"
# # }

resource "local_file" "main-variable-file" {
    content = yamlencode({"variables": {
      #TENANT_ID: data.azuread_client_config.current.tenant_id,
      RESOURCE_GROUP: azurerm_resource_group.resource-group.name,
      AKS_CLUSTER_NAME: azurerm_kubernetes_cluster.aks-cluster.name,
      #APIM_ENDPOINT: data.azurerm_api_management.apim.gateway_url,
      NAMESPACE: "${terraform.workspace}-backstage"
    }})
    filename = "${path.module}/../../overrides/${terraform.workspace}/main-variables.yaml"
}