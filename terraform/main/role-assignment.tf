# Grant AKS user AKS service cluster role on AKS cluster
resource "azurerm_role_assignment" "role-assignment-aks-user-aks" {
  depends_on = [
    azurerm_kubernetes_cluster.aks-cluster
  ]

  principal_id                     = data.azuread_service_principal.aks-user.id #objectId of Terraform Service Principal
  role_definition_name             = "Azure Kubernetes Service Cluster User Role"
  scope                            = azurerm_kubernetes_cluster.aks-cluster.id
  skip_service_principal_aad_check = true
}

# Grant AKS user contributor role on resource group
resource "azurerm_role_assignment" "aks-sp-role-assignment" {
  depends_on = [
      azurerm_resource_group.resource-group
  ]

  principal_id                     = data.azuread_service_principal.aks-user.id #objectId of Terraform Service Principal
  role_definition_name             = "Contributor"
  scope                            = azurerm_resource_group.resource-group.id
  skip_service_principal_aad_check = true
}
