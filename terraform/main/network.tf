# Vnet creation 
resource "azurerm_virtual_network" "vnet" {
  name                = "${terraform.workspace}-${var.prefix}-vnet"
  location            = azurerm_resource_group.resource-group.location
  resource_group_name = azurerm_resource_group.resource-group.name
  address_space       = [var.vnet_cidr]

  tags = {
    ENV = "${terraform.workspace}"
  }
}

# Subnet Creation
resource "azurerm_subnet" "subnet" {
  name                 = "${terraform.workspace}-${var.prefix}-subnet"
  resource_group_name  = azurerm_resource_group.resource-group.name
  address_prefixes     = [var.subnet_cidr]
  virtual_network_name = azurerm_virtual_network.vnet.name
}

# Vnet peering from AKS to APIM vnet
resource "azurerm_virtual_network_peering" "aks-apim-peer" {
  name                      = "${terraform.workspace}-${var.prefix}-aks-apim-peer"
  resource_group_name       = azurerm_resource_group.resource-group.name
  virtual_network_name      = azurerm_virtual_network.vnet.name
  remote_virtual_network_id = data.azurerm_virtual_network.apim-vnet.id
}

# Vnet peering from APIM to AKS vnet
resource "azurerm_virtual_network_peering" "apim-aks-peer" {
  name                      = "${terraform.workspace}-${var.prefix}-apim-aks-peer"
  resource_group_name       = "poc_rg"
  virtual_network_name      = "aks_vnet_2000"
  remote_virtual_network_id = azurerm_virtual_network.vnet.id
}