# resource "random_string" "storage-account-name" {
#   #count = var.storage-account-nos
  
#   length  = 12
#   special = false
#   upper   = false
# }

# # Generate Random Password
# resource "random_password" "psql-password" {
#   length  = 16
#   special = true
# }


# Generate Random Password
resource "random_password" "aks-sp-password" {
  length  = 16
  special = true
}

# Generate Random Password
resource "random_password" "gitlab-sp-password" {
  length  = 16
  special = true
}