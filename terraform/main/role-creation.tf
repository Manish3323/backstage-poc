# # Create custom role
# data "azurerm_subscription" "primary" {
# }

# resource "azurerm_role_definition" "custom-role" {
#   name        = "${terraform.workspace}-backstage-join-pub-ip"
#   scope       = data.azurerm_subscription.primary.id
#   description = "Custom role for linking LB to publicIP"

#   permissions {
#     actions = ["Microsoft.Network/publicIPAddresses/join/action"]
#   }

#   assignable_scopes = [for ip in azurerm_public_ip.public-ip[*]: ip.id]
# }