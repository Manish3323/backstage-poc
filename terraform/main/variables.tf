variable "SUBSCRIPTION_ID" {
  type    = string
  default = "3344a922-f246-4f27-a6f1-3c85586f7b99"
}

variable "TENANT_ID" {
  type    = string
  default = "6fecd065-cb31-41b4-985c-60f2fdf1720f"
}

variable "CLIENT_ID" {
  type    = string
  default = "6ae2af55-77a9-4f1a-9bda-d9091301f3e6"
}

variable "CLIENT_SECRET" {
  type    = string
}

variable "storage-account-nos" {
  type = string
  default = 2
}

variable "prefix" {
  type    = string
  default = "backstage"
}

variable "region" {}

variable "acr-sku" {
  type    = string
  default = "Basic"
}

variable "ip-sku" {
  type    = string
  default = "Standard"
}

variable "allocation_method" {
  type    = string
  default = "Static"
}

variable "psql_user" {
  type    = string
  default = "admin_user"
}

variable "vnet_cidr" {}
variable "subnet_cidr" {}