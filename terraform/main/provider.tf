provider "azurerm" {
  features {}

  subscription_id = var.SUBSCRIPTION_ID
  tenant_id       = var.TENANT_ID
  client_id       = var.CLIENT_ID
  client_secret   = var.CLIENT_SECRET
}

# provider "kubernetes" {
#   host                   = azurerm_kubernetes_cluster.aks-cluster.kube_config.0.host
#   username               = azurerm_kubernetes_cluster.aks-cluster.kube_config.0.username
#   password               = azurerm_kubernetes_cluster.aks-cluster.kube_config.0.password
#   client_certificate     = base64decode(azurerm_kubernetes_cluster.aks-cluster.kube_config.0.client_certificate)
#   client_key             = base64decode(azurerm_kubernetes_cluster.aks-cluster.kube_config.0.client_key)
#   cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.aks-cluster.kube_config.0.cluster_ca_certificate)

# }