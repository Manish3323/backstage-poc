#data "azuread_client_config" "current" {}

data "azurerm_container_registry" "data-acr" {
  resource_group_name = "common-${var.prefix}-rg"
  name = "common${var.prefix}ACR"
}

data "azuread_service_principal" "aks-user" {
  display_name = "common-backstage-aks-user"
}

data "azuread_service_principal" "gitlab-user" {
  display_name = "common-backstage-gitlab-user"
}

# data "azurerm_api_management" "apim" {
#   name                = "backstagepocapim"
#   resource_group_name = "poc_rg"
# }

data "azurerm_virtual_network" "apim-vnet" {
  name                = "aks_vnet_2000"
  resource_group_name = "poc_rg"
}