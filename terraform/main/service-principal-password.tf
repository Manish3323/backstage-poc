resource "azuread_service_principal_password" "aks-sp-password" {
  service_principal_id = data.azuread_service_principal.aks-user.object_id
}

resource "azuread_service_principal_password" "gitlab-sp-password" {
  service_principal_id = data.azuread_service_principal.gitlab-user.object_id
}
